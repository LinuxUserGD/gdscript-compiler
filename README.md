# GDScript Compiler
This is just a prototype and there are still things to do,
also my Rust skill is basic zero (this is my first project).

## Installation
You need to have [Rust](https://www.rust-lang.org/tools/install) and LLVM 14 installed
if your distros LLVM not working then uncompress LLVM.tar.gz set LLVM_SYS_140_PREFIX to it

```
git clone https://gitlab.com/the-SSD/gdscript-compiler.git
cd gdscript-compiler
cargo run
```

# Useage
compiler is going to compile what is in ./src/main.gd to ./output/gdscript.o
then link with lib.c and output to executable


## Helpful links
[GDScript code examples](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_basics.html)

## Helpful tips
### Tip 1 faster compilation
to make compiling faster install mold and in file `~/.cargo/config` put this
```
[build]
rustflags = [
  "-C", "link-arg=-fuse-ld=mold",
]
```

Now rust will use mold linker witch is faster
This is not officially supported!!!