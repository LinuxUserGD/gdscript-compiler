use std::collections::HashMap;
use std::path::Path;

use inkwell::basic_block::BasicBlock;
use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::module::{Linkage, Module};
use inkwell::targets::{self, InitializationConfig, Target, TargetMachine};
use inkwell::{FloatPredicate, IntPredicate, OptimizationLevel};

use inkwell::passes::PassManager;
use inkwell::types::{BasicMetadataTypeEnum, BasicTypeEnum, FunctionType};
use inkwell::values::{
    BasicMetadataValueEnum, BasicValueEnum, FunctionValue, InstructionOpcode, PointerValue,
};

use crate::parser::{AstNode, Parser};
use crate::tokenizer::Tokenizer;

pub struct Compiler<'ctx, 'a> {
    pub context: &'ctx Context,
    pub builder: &'a Builder<'ctx>,
    pub module: &'a Module<'ctx>,
    pub fpm: &'a PassManager<FunctionValue<'ctx>>,

    defualt_function_args: HashMap<String, Vec<BasicValueEnum<'ctx>>>,
    global_variables: HashMap<String, PointerValue<'ctx>>,
    break_branch: Option<BasicBlock<'ctx>>,
    continue_branch: Option<BasicBlock<'ctx>>,
}

impl<'ctx, 'a> Compiler<'ctx, 'a> {
    pub fn new(
        use_optimizations: bool,
        context: &'ctx Context,
        builder: &'a Builder<'ctx>,
        module: &'a Module<'ctx>,
        fpm: &'a PassManager<FunctionValue<'ctx>>,
    ) -> Compiler<'ctx, 'a> {
        if use_optimizations {
            fpm.add_instruction_combining_pass();
            fpm.add_reassociate_pass();
            fpm.add_gvn_pass();
            fpm.add_cfg_simplification_pass();
            fpm.add_basic_alias_analysis_pass();
            fpm.add_promote_memory_to_register_pass();
            fpm.add_instruction_combining_pass();
            fpm.add_reassociate_pass();
        }

        fpm.initialize();

        Compiler {
            context,
            builder,
            module,
            fpm,

            defualt_function_args: HashMap::new(),
            global_variables: HashMap::new(),
            break_branch: None,
            continue_branch: None,
        }
    }

    pub fn add_gdextension(&mut self) {
        
        let code = std::fs::read_to_string("./src/main.gd").unwrap();

        let tokens = Tokenizer::new(&code).parse();

        let mut nodes = Parser::new(&tokens).parse();

        let init_function = AstNode::Function { name: "gdextention".to_owned(), return_type: "void".to_owned(), args: [

        ].to_vec(), body: [

        ].to_vec() };

        nodes.push(init_function);
        self.compile_all(&nodes);

        println!("gdextension isn't supported this is just a prototype");
    }

    pub fn compile_all(&mut self, nodes: &Vec<AstNode>) {
        self.define_functions(nodes);

        for node in nodes {
            match node {
                AstNode::Function {
                    name,
                    return_type,
                    args,
                    body,
                } => {
                    self.compile_func(name, return_type, args, body);
                }

                AstNode::DefineFunction {
                    name,
                    return_type,
                    args,
                } => {
                    self.compile_defanition(name, return_type, args);
                }

                n => {
                    panic!("only functions and function definitions are currently supported here \n{:?}", n);
                }
            }
        }
    }

    fn define_functions(&mut self, nodes: &Vec<AstNode>) {
        for node in nodes {
            match node {
                AstNode::Function {
                    name,
                    return_type,
                    args,
                    body: _,
                } => {
                    let mut param_types: Vec<BasicMetadataTypeEnum> = vec![];

                    for arg in args {
                        param_types.push(self.get_basic_metadata_type(&arg.arg_type))
                    }

                    let function_type = self.get_func_type(return_type.to_string(), param_types);

                    let function = self.module.add_function(
                        name.as_str(),
                        function_type,
                        Some(Linkage::External),
                    );

                    for (i, arg) in function.get_param_iter().enumerate() {
                        arg.set_name(args[i].name.as_str());
                    }

                    let mut func_args: Vec<BasicValueEnum> = Vec::new();
                    for arg in args {
                        let mut local_variables = self.global_variables.clone();
                        let arg =
                            self.compile_basic_node(&arg.body, &function, &mut local_variables);

                        match arg {
                            Some(arg) => func_args.push(arg),
                            None => todo!(),
                        }
                    }

                    self.defualt_function_args
                        .insert(name.to_string(), func_args);
                }
                _ => {}
            }
        }
    }

    pub fn write_to_file(&mut self, op_level: OptimizationLevel, path: &str) {
        let default_triple = TargetMachine::get_default_triple();
        self.module.set_triple(&default_triple);

        let config = InitializationConfig {
            asm_parser: true,
            asm_printer: true,
            base: true,
            disassembler: true,
            info: true,
            machine_code: true,
        };

        Target::initialize_all(&config);

        let target = Target::from_triple(&default_triple).unwrap();

        let cpu = "generic";
        let features = "";

        let machine_target = target
            .create_target_machine(
                &default_triple,
                cpu,
                features,
                op_level,
                targets::RelocMode::Default,
                targets::CodeModel::Default,
            )
            .unwrap();
        self.module
            .set_data_layout(&machine_target.get_target_data().get_data_layout());

        machine_target
            .write_to_file(self.module, targets::FileType::Object, Path::new(path))
            .unwrap();
    }

    pub fn print_ir(&self) {
        for function in self.module.get_functions() {
            function.print_to_stderr();
            println!();
        }
    }

    pub fn run_with_jit(&mut self) -> i64 {
        let mut func_name = "";

        let functions: Vec<FunctionValue> = self.module.get_functions().collect();
        for function in &functions {
            let name = function.get_name().to_str().unwrap();

            if func_name != "main" {
                func_name = name;
            }
        }

        let ee = self
            .module
            .create_jit_execution_engine(inkwell::OptimizationLevel::None)
            .unwrap();

        // return type isn't always i64
        let maybe_fn = unsafe { ee.get_function::<unsafe extern "C" fn() -> i64>(func_name) };
        let compiled_fn = match maybe_fn {
            Ok(f) => f,
            Err(err) => {
                println!("!> Error during execution: {:?}", err);
                panic!()
                //continue;
            }
        };

        let result;
        unsafe {
            result = compiled_fn.call();
            println!("result => {}", result);
        }
        result
    }

    fn get_basic_metadata_type(&self, type_string: &str) -> BasicMetadataTypeEnum<'ctx> {
        match type_string {
            "float" => BasicMetadataTypeEnum::FloatType(self.context.f64_type()),
            "int" => BasicMetadataTypeEnum::IntType(self.context.i64_type()),

            _ => {
                todo!()
            }
        }
    }

    fn get_type(&self, type_string: String) -> BasicTypeEnum<'ctx> {
        match type_string.as_str() {
            "float" => BasicTypeEnum::FloatType(self.context.f64_type()),
            "int" => BasicTypeEnum::IntType(self.context.i64_type()),
            "bool" => BasicTypeEnum::IntType(self.context.bool_type()),
            _ => todo!(),
        }
    }

    fn get_func_type(
        &self,
        type_string: String,
        param_types: Vec<BasicMetadataTypeEnum<'ctx>>,
    ) -> FunctionType<'ctx> {
        match type_string.as_str() {
            "float" => self
                .context
                .f64_type()
                .fn_type(param_types.as_slice(), false),
            "int" => self
                .context
                .i64_type()
                .fn_type(param_types.as_slice(), false),
            "void" => self
                .context
                .void_type()
                .fn_type(param_types.as_slice(), false),
            "bool" => self
                .context
                .bool_type()
                .fn_type(param_types.as_slice(), false),
            _ => todo!(),
        }
    }

    fn compile_math(
        &self,
        lhs: &BasicValueEnum<'ctx>,
        rhs: &BasicValueEnum<'ctx>,
        op: &str,
    ) -> BasicValueEnum<'ctx> {
        let mut is_float = false;

        let mut lhs_float = self.context.f64_type().const_zero();
        let mut lhs_int = self.context.i64_type().const_zero();

        let mut rhs_float = self.context.f64_type().const_zero();
        let mut rhs_int = self.context.i64_type().const_zero();

        if let BasicValueEnum::FloatValue(float_val) = lhs {
            is_float = true;
            lhs_float = *float_val;
        } else if let BasicValueEnum::IntValue(int_val) = lhs {
            lhs_int = *int_val;
        } else {
            panic!("unknown type {}", lhs)
        }

        if let BasicValueEnum::FloatValue(float_val) = rhs {
            rhs_float = *float_val;

            if !is_float {
                lhs_float = self
                    .builder
                    .build_cast(
                        InstructionOpcode::SIToFP,
                        lhs_int,
                        self.context.f64_type(),
                        "cast",
                    )
                    .into_float_value();
            }
            is_float = true;
        } else if let BasicValueEnum::IntValue(int_val) = rhs {
            rhs_int = *int_val;

            if is_float {
                rhs_float = self
                    .builder
                    .build_cast(
                        InstructionOpcode::SIToFP,
                        rhs_int,
                        self.context.f64_type(),
                        "cast",
                    )
                    .into_float_value();
            }
        } else {
            panic!("unknown type {}", rhs)
        }

        if is_float {
            let lhs = lhs_float;
            let rhs = rhs_float;

            match op {
                "+" => BasicValueEnum::FloatValue(self.builder.build_float_add(lhs, rhs, "add")),
                "-" => BasicValueEnum::FloatValue(self.builder.build_float_sub(lhs, rhs, "sub")),
                "*" => BasicValueEnum::FloatValue(self.builder.build_float_mul(lhs, rhs, "mul")),
                "/" => BasicValueEnum::FloatValue(self.builder.build_float_div(lhs, rhs, "div")),
                "<" => BasicValueEnum::IntValue(self.builder.build_float_compare(
                    FloatPredicate::ULT,
                    lhs,
                    rhs,
                    "cmp_less",
                )),
                ">" => BasicValueEnum::IntValue(self.builder.build_float_compare(
                    FloatPredicate::UGT,
                    lhs,
                    rhs,
                    "cmp_more",
                )),
                "==" => BasicValueEnum::IntValue(self.builder.build_float_compare(
                    FloatPredicate::OEQ,
                    lhs,
                    rhs,
                    "cmp_equals",
                )),
                "!=" => BasicValueEnum::IntValue(self.builder.build_float_compare(
                    FloatPredicate::ONE,
                    lhs,
                    rhs,
                    "cmp_not",
                )),
                "<=" => BasicValueEnum::IntValue(self.builder.build_float_compare(
                    FloatPredicate::ULE,
                    lhs,
                    rhs,
                    "cmp_less_or_equal",
                )),
                ">=" => BasicValueEnum::IntValue(self.builder.build_float_compare(
                    FloatPredicate::UGE,
                    lhs,
                    rhs,
                    "cmp_more_or_equal",
                )),

                _ => todo!(),
            }
        } else {
            let lhs = lhs_int;
            let rhs = rhs_int;

            match op {
                "+" => BasicValueEnum::IntValue(self.builder.build_int_add(lhs, rhs, "add")),
                "-" => BasicValueEnum::IntValue(self.builder.build_int_sub(lhs, rhs, "sub")),
                "*" => BasicValueEnum::IntValue(self.builder.build_int_mul(lhs, rhs, "mul")),
                "/" => BasicValueEnum::IntValue(self.builder.build_int_signed_div(lhs, rhs, "div")),
                "<" => BasicValueEnum::IntValue(self.builder.build_int_compare(
                    IntPredicate::ULT,
                    lhs,
                    rhs,
                    "cmp_less",
                )),
                ">" => BasicValueEnum::IntValue(self.builder.build_int_compare(
                    IntPredicate::UGT,
                    lhs,
                    rhs,
                    "cmp_more",
                )),
                "==" => BasicValueEnum::IntValue(self.builder.build_int_compare(
                    IntPredicate::EQ,
                    lhs,
                    rhs,
                    "cmp_equals",
                )),
                "!=" => BasicValueEnum::IntValue(self.builder.build_int_compare(
                    IntPredicate::NE,
                    lhs,
                    rhs,
                    "cmp_not",
                )),
                "<=" => BasicValueEnum::IntValue(self.builder.build_int_compare(
                    IntPredicate::ULE,
                    lhs,
                    rhs,
                    "cmp_less_or_equal",
                )),
                ">=" => BasicValueEnum::IntValue(self.builder.build_int_compare(
                    IntPredicate::UGE,
                    lhs,
                    rhs,
                    "cmp_more_or_equal",
                )),
                "&&" => BasicValueEnum::IntValue(self.builder.build_and(lhs, rhs, "and")),
                "||" => BasicValueEnum::IntValue(self.builder.build_or(lhs, rhs, "or")),
                "^" => BasicValueEnum::IntValue(self.builder.build_xor(lhs, rhs, "xor")),
                "!" => BasicValueEnum::IntValue(self.builder.build_not(rhs, "not")),

                _ => todo!(),
            }
        }
    }

    /// Creates a new stack allocation instruction in the entry block of the function.
    fn create_entry_block_alloca(
        &self,
        func: &FunctionValue,
        name: &str,
        ty: BasicTypeEnum<'ctx>,
    ) -> PointerValue<'ctx> {
        let builder = self.context.create_builder();

        let entry = func.get_first_basic_block().unwrap();

        match entry.get_first_instruction() {
            Some(first_instr) => builder.position_before(&first_instr),
            None => builder.position_at_end(entry),
        }

        builder.build_alloca(ty, name)
    }

    fn compile_defanition(
        &mut self,
        name: &String,
        return_type: &String,
        args: &Vec<crate::parser::Arg>,
    ) {
        let mut param_types: Vec<BasicMetadataTypeEnum> = vec![];

        for arg in args {
            param_types.push(self.get_basic_metadata_type(&arg.arg_type))
        }

        let function_type = self.get_func_type(return_type.to_string(), param_types);

        let function =
            self.module
                .add_function(name.as_str(), function_type, Some(Linkage::External));

        for (i, arg) in function.get_param_iter().enumerate() {
            arg.set_name(args[i].name.as_str());
        }

        let mut func_args: Vec<BasicValueEnum> = Vec::new();
        for arg in args {
            let mut local_variables = self.global_variables.clone();
            let arg = self.compile_basic_node(&arg.body, &function, &mut local_variables);

            match arg {
                Some(arg) => func_args.push(arg),
                None => todo!(),
            }
        }

        self.defualt_function_args
            .insert(name.to_string(), func_args);
    }

    fn compile_func(
        &mut self,
        name: &String,
        return_type: &String,
        args: &Vec<crate::parser::Arg>,
        body: &Vec<AstNode>,
    ) -> FunctionValue {
        let function = self.module.get_function(name).unwrap();

        let mut local_variables = self.global_variables.clone();

        let entry = self.context.append_basic_block(function, "entry");

        self.builder.position_at_end(entry);

        for (i, arg) in function.get_param_iter().enumerate() {
            let arg_name = args[i].name.to_string();
            let arg_type = args[i].arg_type.to_string();

            let builder = self.context.create_builder();

            let entry = function.get_first_basic_block().unwrap();

            match entry.get_first_instruction() {
                Some(first_instr) => builder.position_before(&first_instr),
                None => builder.position_at_end(entry),
            }

            let alloca = builder.build_alloca(self.get_type(arg_type), name);

            self.builder.build_store(alloca, arg);

            local_variables.insert(arg_name, alloca);
        }

        //let mut has_return = false;
        // compile body

        for node in body {
            self.compile_basic_node(node, &function, &mut local_variables);

            if let AstNode::Return { body: _ } = node {
                //has_return = true;
                break;
            }
        }
        if return_type == "void" {
            self.builder.build_return(None);
        }

        if function.verify(true) {
            self.fpm.run_on(&function);
        } else {
            println!();
            function.print_to_stderr();
            println!();
            panic!("Function verification faild");
        }

        function
    }

    pub fn compile_basic_node(
        &mut self,
        node: &AstNode,
        function: &FunctionValue<'ctx>,
        variables: &mut HashMap<String, PointerValue<'ctx>>,
    ) -> Option<BasicValueEnum<'ctx>> {
        match node {
            AstNode::Number(num) => {
                let value = self.context.i64_type().const_int(*num, true);
                return Some(BasicValueEnum::IntValue(value));
            }

            AstNode::FloatingPointNumber(num) => {
                let value = self.context.f64_type().const_float(*num);
                return Some(BasicValueEnum::FloatValue(value));
            }

            AstNode::Binary { op, left, right } => {
                let left_val = self.compile_basic_node(left, function, variables).unwrap();

                let right_val = self.compile_basic_node(right, function, variables).unwrap();

                Some(self.compile_math(&left_val, &right_val, op))
            }

            AstNode::Function {
                name: _,
                return_type: _,
                args: _,
                body: _,
            } => {
                panic!("no func here allowed");
            }

            AstNode::DefVariable {
                name,
                var_type,
                body,
            } => {
                let compiled_body = self.compile_basic_node(body, function, variables).unwrap();

                let var_type_enum: BasicTypeEnum;
                if var_type == "unknown" {
                    if let BasicValueEnum::FloatValue(_) = compiled_body {
                        var_type_enum = BasicTypeEnum::FloatType(self.context.f64_type());
                    } else if let BasicValueEnum::IntValue(_) = compiled_body {
                        var_type_enum = BasicTypeEnum::IntType(self.context.i64_type());
                    } else {
                        panic!("unknown type {}", compiled_body)
                    }
                } else {
                    var_type_enum = self.get_type(var_type.to_string());
                }

                let alloca = self.create_entry_block_alloca(function, name, var_type_enum);

                self.builder.build_store(alloca, compiled_body);
                variables.insert(name.to_string(), alloca);

                None
            }

            AstNode::Call { func_name, args } => {
                match self.module.get_function(func_name.as_str()) {
                    Some(func) => {
                        let mut compiled_args = Vec::with_capacity(args.len());

                        for arg in args {
                            compiled_args
                                .push(self.compile_basic_node(arg, function, variables).unwrap());
                        }

                        if func.get_params().len() > args.len() {
                            let len = func.get_params().len() - args.len();
                            let start = args.len();

                            let defualt_args = &self.defualt_function_args[func_name];

                            for i in start..len {
                                compiled_args.push(defualt_args[i]);
                            }
                        }

                        let argsv: Vec<BasicMetadataValueEnum> = compiled_args
                            .iter()
                            .by_ref()
                            .map(|&val| val.into())
                            .collect();

                        let val = self
                            .builder
                            .build_call(func, argsv.as_slice(), "call")
                            .try_as_basic_value()
                            .left();
                        val
                    }
                    None => panic!(
                        "Unknown function {} or function is below current call",
                        func_name
                    ),
                }
            }

            AstNode::If {
                cond,
                consequence,
                alternative,
            } => {
                let compiled_cond = self
                    .compile_basic_node(cond, function, variables)
                    .unwrap()
                    .into_int_value();

                // build branch
                let then_bb = self.context.append_basic_block(*function, "then");
                let else_bb = self.context.append_basic_block(*function, "else");
                let cont_bb = self.context.append_basic_block(*function, "ifcont");

                self.builder
                    .build_conditional_branch(compiled_cond, then_bb, else_bb);

                // build then block
                self.builder.position_at_end(then_bb);

                let mut local_variables = variables.clone();
                let mut has_return = false;
                for node in consequence {
                    match node {
                        AstNode::Return { body: _ } => {
                            self.compile_basic_node(node, function, &mut local_variables);
                            has_return = true;
                            break;
                        }
                        AstNode::Break {} => {
                            self.compile_basic_node(node, function, &mut local_variables);
                            has_return = true;
                            break;
                        }

                        _ => {
                            self.compile_basic_node(node, function, &mut local_variables);
                        }
                    }
                }
                if !has_return {
                    self.builder.build_unconditional_branch(cont_bb);
                }

                // build else block
                self.builder.position_at_end(else_bb);

                let mut local_variables = variables.clone();
                has_return = false;
                for node in alternative {
                    self.compile_basic_node(node, function, &mut local_variables);
                    if let AstNode::Return { body: _ } = node {
                        has_return = true;
                        break;
                    }
                }
                if !has_return {
                    self.builder.build_unconditional_branch(cont_bb);
                }

                // emit merge block
                self.builder.position_at_end(cont_bb);

                None
            }

            AstNode::While { cond, body } => {
                let check_bb = self.context.append_basic_block(*function, "while_check");
                let while_bb = self.context.append_basic_block(*function, "while_body");
                let continue_bb = self.context.append_basic_block(*function, "while_continue");

                self.builder.build_unconditional_branch(check_bb);

                self.builder.position_at_end(check_bb);

                let compiled_cond = self.compile_basic_node(cond, function, variables).unwrap();
                self.builder.build_conditional_branch(
                    compiled_cond.into_int_value(),
                    while_bb,
                    continue_bb,
                );

                self.builder.position_at_end(while_bb);

                self.break_branch = Some(continue_bb);
                self.continue_branch = Some(check_bb);

                let mut local_variables = variables.clone();
                for node in body {
                    self.compile_basic_node(node, function, &mut local_variables);
                }

                self.builder.build_unconditional_branch(check_bb);

                self.builder.position_at_end(continue_bb);

                None
            }

            AstNode::SetVariable { name, op, body } => {
                let mut new_val = self.compile_basic_node(body, function, variables).unwrap();

                let var = variables
                    .get(name.as_str())
                    .ok_or("Undefined variable.")
                    .unwrap();

                if op != "=" {
                    let var_val = self.builder.build_load(*var, name.as_str());

                    match op.as_str() {
                        "+=" => {
                            new_val = self.compile_math(&var_val, &new_val, "+");
                        }

                        "-=" => {
                            new_val = self.compile_math(&var_val, &new_val, "-");
                        }

                        "*=" => {
                            new_val = self.compile_math(&var_val, &new_val, "*");
                        }

                        "/=" => {
                            new_val = self.compile_math(&var_val, &new_val, "/");
                        }

                        _ => {}
                    }
                }

                self.builder.build_store(*var, new_val);

                None
            }

            AstNode::GetVariable { name } => {
                let val: BasicValueEnum = match name.as_str() {
                    "true" => {
                        BasicValueEnum::IntValue(self.context.bool_type().const_int(1, false))
                    }
                    "false" => BasicValueEnum::IntValue(self.context.bool_type().const_zero()),
                    "PI" => BasicValueEnum::FloatValue(
                        self.context.f64_type().const_float(std::f64::consts::PI),
                    ),
                    "TAU" => BasicValueEnum::FloatValue(
                        self.context.f64_type().const_float(std::f64::consts::TAU),
                    ),
                    "NAN" => BasicValueEnum::FloatValue(
                        self.context.f64_type().const_float(std::f64::NAN),
                    ),
                    "INF" => BasicValueEnum::FloatValue(
                        self.context.f64_type().const_float(std::f64::INFINITY),
                    ),
                    "null" => {
                        panic!("null isn't supported yet");
                    }

                    _ => {
                        let var = variables.get(name.as_str());

                        let var = match var {
                            Some(s) => s,
                            None => panic!("unknown var {}", name),
                        };

                        self.builder.build_load(*var, name.as_str())
                    }
                };

                Some(val)
            }

            AstNode::Return { body } => {
                let compiled_body = self.compile_basic_node(body, function, variables).unwrap();
                self.builder.build_return(Some(&compiled_body));

                None
            }

            AstNode::None => {
                return Some(BasicValueEnum::IntValue(
                    self.context.i64_type().const_int(0, false),
                ));
            }

            AstNode::For {
                var_name,
                start,
                end,
                step,
                body,
            } => {
                let mut local_variables = variables.clone();

                let start_alloca = self.create_entry_block_alloca(
                    function,
                    var_name,
                    BasicTypeEnum::IntType(self.context.i64_type()),
                );
                let start = self
                    .compile_basic_node(start, function, &mut local_variables)
                    .unwrap()
                    .into_int_value();

                self.builder.build_store(start_alloca, start);

                local_variables.insert(var_name.to_owned(), start_alloca);

                variables.insert(var_name.to_owned(), start_alloca);

                let loop_bb = self.context.append_basic_block(*function, "loop");

                self.builder.build_unconditional_branch(loop_bb);
                self.builder.position_at_end(loop_bb);

                let after_bb = self.context.append_basic_block(*function, "afterloop");
                self.break_branch = Some(after_bb);

                // compile body
                for node in body {
                    self.compile_basic_node(node, function, &mut local_variables);
                }

                let step = self
                    .compile_basic_node(step, function, &mut local_variables)
                    .unwrap()
                    .into_int_value();
                let end = self
                    .compile_basic_node(end, function, &mut local_variables)
                    .unwrap()
                    .into_int_value();

                let curr_var = self
                    .builder
                    .build_load(start_alloca, "load")
                    .into_int_value();
                let next_var = self.builder.build_int_add(curr_var, step, "loop_add");

                self.builder.build_store(start_alloca, next_var);

                let end_cond =
                    self.builder
                        .build_int_compare(IntPredicate::ULT, next_var, end, "loopcond");

                self.builder
                    .build_conditional_branch(end_cond, loop_bb, after_bb);
                self.builder.position_at_end(after_bb);

                self.break_branch = None;
                None
            }

            AstNode::Break => {
                self.builder.build_unconditional_branch(self.break_branch?);
                None
            }

            AstNode::Continue => {
                self.builder
                    .build_unconditional_branch(self.continue_branch?);
                None
            }

            node => {
                panic!("what is this: {:?}", node);
            }
        }
    }
}
