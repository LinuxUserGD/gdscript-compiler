#define print(i: int)

func main() -> int:
    benchmark()
    return 0

func benchmark() -> int:
    var n1 := 0
    var n2 := 1
    for i in range(0, 1000000): # 1 000 000
        var n := n2
        n2 = n2 + n1
        n1 = n

#    print(n2)
    return n2


func test():
    pass