use crate::tokenizer::{RsvIdentifier, Token};

#[derive(Debug, Clone, PartialEq)]
pub enum AstNode {

    Class {
        body: Vec<AstNode>,
    },

    Binary {
        op: String,
        left: Box<AstNode>,
        right: Box<AstNode>,
    },

    Call {
        func_name: String,
        args: Vec<AstNode>,
    },

    If {
        cond: Box<AstNode>,
        consequence: Vec<AstNode>,
        alternative: Vec<AstNode>,
    },

    For {
        var_name: String,
        start: Box<AstNode>,
        end: Box<AstNode>,
        step: Box<AstNode>,
        body: Vec<AstNode>,
    },

    While {
        cond: Box<AstNode>,
        body: Vec<AstNode>,
    },
    Break,
    Continue,

    Number(u64),
    FloatingPointNumber(f64),

    DefVariable {
        name: String,
        var_type: String,
        body: Box<AstNode>,
    },

    GetVariable {
        name: String,
    },

    SetVariable {
        name: String,
        op: String,
        body: Box<AstNode>,
    },

    Function {
        name: String,
        return_type: String,
        args: Vec<Arg>,
        body: Vec<AstNode>,
    },

    DefineFunction {
        name: String,
        return_type: String,
        args: Vec<Arg>,
    },

    Return {
        body: Box<AstNode>,
    },

    Error,
    None,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Arg {
    pub name: String,
    pub arg_type: String,
    pub body: Box<AstNode>,
}
pub struct Parser {
    tokens: Vec<Token>,
    pos: usize,
}

impl Parser {
    pub fn new(tokens: &[Token]) -> Parser {
        Parser {
            tokens: tokens.to_vec(),
            pos: 0,
        }
    }

    fn peek(&self) -> Token {
        self.tokens[self.pos].clone()
    }

    fn next(&mut self) -> Token {
        self.pos += 1;
        self.tokens[self.pos].clone()
    }

    pub fn parse(&mut self) -> Vec<AstNode> {
        let mut return_nodes = Vec::<AstNode>::new();

        loop {
            let start = self.pos;

            if self.tokens.len() <= self.pos {
                break;
            }

            if let Token::EOF = self.peek() {
                break;
            }

            let node = self.parse_node();

            if start == self.pos {
                self.pos += 1;
            }

            if let Some(node) = node {
                return_nodes.push(node)
            }
        }
        return_nodes
    }

    fn parse_node(&mut self) -> Option<AstNode> {
        match self.peek() {
            Token::Reserved(rsv) => match rsv {
                RsvIdentifier::Func => Some(self.parse_func()),
                RsvIdentifier::Define => Some(self.parse_define()),

                RsvIdentifier::Var => Some(self.parse_var()),

                RsvIdentifier::If => Some(self.parse_if()),

                RsvIdentifier::Elif => Some(self.parse_if()),

                RsvIdentifier::While => Some(self.parse_while()),

                RsvIdentifier::Return => {
                    self.next();
                    Some(AstNode::Return {
                        body: Box::new(self.parse_expr()),
                    })
                }

                RsvIdentifier::For => Some(self.parse_for()),

                RsvIdentifier::Break => Some(AstNode::Break),
                RsvIdentifier::Continue => Some(AstNode::Continue),

                RsvIdentifier::Pass => None,
                _ => None,
            },

            Token::Identifier(identifier) => {
                let next = self.next();
                if let Token::Special(sp) = next {
                    match sp.as_str() {
                        "=" | "+=" | "-=" | "*=" | "/=" => {
                            self.next();
                            let body = self.parse_expr();

                            return Some(AstNode::SetVariable {
                                name: identifier,
                                op: sp,
                                body: Box::new(body),
                            });
                        }
                        _ => {}
                    }
                }
                self.pos -= 1;
                Some(self.parse_expr())
            }

            //Token::Comment(_) => None,
            Token::NewLine => None,
            _ => Some(self.parse_expr()),
        }
    }

    fn parse_for(&mut self) -> AstNode {
        if let Token::Identifier(var_name) = self.next() {
            if self.next() != Token::Reserved(RsvIdentifier::In) {
                panic!("synatx error");
            }

            let mut start;
            let end;
            let step;

            if Token::Identifier("range".to_string()) == self.next() {
                if self.next() != Token::Special("(".to_string()) {
                    panic!("synatx error");
                }

                start = self.parse_expr();
                self.pos -= 1;

                if Token::Special(",".to_string()) == self.peek() {
                    self.next();
                    end = self.parse_expr();

                    if Token::Special(",".to_string()) == self.peek() {
                        self.next();
                        step = self.parse_expr();
                    } else {
                        step = AstNode::Number(1);
                    }
                } else {
                    end = start;
                    start = AstNode::Number(0);
                    step = AstNode::Number(1);
                }

                self.next(); //skip )
            } else {
                let expr = self.parse_expr();
                start = AstNode::Number(0);
                end = expr;
                step = AstNode::Number(1);
            }

            self.next(); //skip :
            self.next(); //skip NewLine again???

            let mut indent = 0;
            loop {
                if let Token::Ident = self.peek() {
                    indent += 1;
                    self.next();
                } else if let Token::DeIdent = self.peek() {
                    indent -= 1;
                    self.next();
                } else {
                    break;
                }
            }

            let mut body = Vec::<AstNode>::new();

            loop {
                if indent <= 0 {
                    break;
                }

                match self.parse_node() {
                    Some(node) => body.push(node),
                    None => {}
                }

                self.next();
                loop {
                    if let Token::Ident = self.peek() {
                        indent += 1;
                        self.next();
                    } else if let Token::DeIdent = self.peek() {
                        indent -= 1;
                        self.next();
                    } else {
                        break;
                    }
                }

                if indent <= 0 {
                    break;
                }
            }

            AstNode::For {
                var_name,
                start: Box::new(start),
                end: Box::new(end),
                step: Box::new(step),
                body,
            }
        } else {
            panic!();
        }
    }

    fn parse_while(&mut self) -> AstNode {
        self.next();
        let cond = self.parse_expr();
        self.next();
        self.next();

        let mut indent = 0;
        loop {
            if let Token::Ident = self.peek() {
                indent += 1;
                self.next();
            } else if let Token::DeIdent = self.peek() {
                indent -= 1;
                self.next();
            } else {
                break;
            }
        }

        let mut body = Vec::<AstNode>::new();

        loop {
            if indent <= 0 {
                break;
            }

            match self.parse_node() {
                Some(node) => body.push(node),
                None => {}
            }

            self.next();
            loop {
                if let Token::Ident = self.peek() {
                    indent += 1;
                    self.next();
                } else if let Token::DeIdent = self.peek() {
                    indent -= 1;
                    self.next();
                } else {
                    break;
                }
            }

            if indent <= 0 {
                break;
            }
        }

        AstNode::While {
            cond: Box::new(cond),
            body,
        }
    }

    fn parse_if(&mut self) -> AstNode {
        self.next();

        let cond = self.parse_expr();

        self.next();
        self.next();

        let mut indent = 0;
        loop {
            if let Token::Ident = self.peek() {
                indent += 1;
                self.next();
            } else if let Token::DeIdent = self.peek() {
                indent -= 1;
                self.next();
            } else {
                break;
            }
        }

        let mut consequence = Vec::<AstNode>::new();
        let mut alternative = Vec::<AstNode>::new();
        loop {
            match self.parse_node() {
                Some(node) => consequence.push(node),
                None => {}
            }

            self.next();
            loop {
                if let Token::Ident = self.peek() {
                    indent += 1;
                    self.next();
                } else if let Token::DeIdent = self.peek() {
                    indent -= 1;
                    self.next();
                } else {
                    break;
                }
            }

            if indent <= 0 {
                break;
            }
        }

        if let Token::Reserved(RsvIdentifier::Else) = self.peek() {
            self.next();
            self.next(); // skip Colon
            self.next(); // skip Newline

            let mut indent = 0;
            loop {
                if let Token::Ident = self.peek() {
                    indent += 1;
                    self.next();
                } else if let Token::DeIdent = self.peek() {
                    indent -= 1;
                    self.next();
                } else {
                    break;
                }
            }

            loop {
                match self.parse_node() {
                    Some(node) => alternative.push(node),
                    None => {}
                }

                self.next();
                loop {
                    if let Token::Ident = self.peek() {
                        indent += 1;
                        self.next();
                    } else if let Token::DeIdent = self.peek() {
                        indent -= 1;
                        self.next();
                    } else {
                        break;
                    }
                }

                if indent <= 0 {
                    break;
                }
            }
        } else if let Token::Reserved(RsvIdentifier::Elif) = self.peek() {
            match self.parse_node() {
                Some(node) => alternative.push(node),
                None => {}
            }
        }

        AstNode::If {
            cond: Box::new(cond),
            consequence,
            alternative,
        }
    }

    fn parse_func(&mut self) -> AstNode {
        let mut return_node: AstNode = AstNode::Error;

        self.next();

        if let Token::Identifier(name) = self.peek() {
            self.next();
            if Token::Special("(".to_string()) == self.peek() {
                let mut args = Vec::new();
                self.next();

                loop {
                    // args

                    if Token::Special(")".to_string()) == self.peek() {
                        break;
                    }

                    if let Token::Identifier(name) = self.peek() {
                        if Token::Special(":".to_string()) == self.next() {
                            if let Token::Identifier(arg_type) = self.next() {
                                let mut body = AstNode::None;

                                self.next();
                                if Token::Special(",".to_string()) == self.peek() {
                                    self.next(); // skip comma
                                } else if Token::Special("=".to_string()) == self.peek() {
                                    self.next(); // skip =
                                    body = self.parse_expr();
                                }

                                args.push(Arg {
                                    name,
                                    arg_type,
                                    body: Box::new(body),
                                });
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }

                self.next();

                let mut return_type = "void".to_string();

                // return type
                if Token::Special("->".to_string()) == self.peek() {
                    self.next();

                    if let Token::Identifier(ret_type) = self.peek() {
                        return_type = ret_type;
                    }
                    self.next();
                    self.next(); //Skip Colon
                } else {
                    self.next();
                    //self.next(); //Skip Colon
                }

                // body

                self.next(); // Skip NewLine

                let mut body = Vec::<AstNode>::new();

                let mut func_indent: i32 = 0;

                loop {
                    // fist ident count

                    if Token::Ident == self.peek() {
                        func_indent += 1;
                        self.next();
                    } else if Token::DeIdent == self.peek() {
                        func_indent -= 1;
                        self.next();
                    } else {
                        break;
                    }
                }

                loop {
                    if self.tokens.len() <= self.pos {
                        break;
                    }

                    if let Token::NewLine = self.peek() {
                        self.next();

                        loop {
                            // ident count

                            if let Token::Ident = self.peek() {
                                func_indent += 1;
                                self.next();
                            } else if let Token::DeIdent = self.peek() {
                                func_indent -= 1;
                                self.next();
                            } else {
                                break;
                            }
                        }

                        if func_indent <= 0 {
                            // exit func
                            break;
                        }
                    } else if let Token::EOF = self.peek() {
                        break;
                    }

                    let start = self.pos;

                    let node = self.parse_node();

                    if self.pos == start {
                        self.next();
                    }

                    match node {
                        Some(node) => body.push(node),
                        None => {}
                    }
                }

                return_node = AstNode::Function {
                    name,
                    return_type,
                    args,
                    body,
                };
            }
        }

        return_node
    }

    fn parse_define(&mut self) -> AstNode {
        let mut return_node: AstNode = AstNode::Error;

        self.next();

        if let Token::Identifier(name) = self.peek() {
            self.next();
            if Token::Special("(".to_string()) == self.peek() {
                let mut args = Vec::new();
                self.next();

                loop {
                    // args

                    if Token::Special(")".to_string()) == self.peek() {
                        break;
                    }

                    if let Token::Identifier(name) = self.peek() {
                        if Token::Special(":".to_string()) == self.next() {
                            if let Token::Identifier(arg_type) = self.next() {
                                let mut body = AstNode::None;

                                self.next();
                                if Token::Special(",".to_string()) == self.peek() {
                                    self.next(); // skip comma
                                } else if Token::Special("=".to_string()) == self.peek() {
                                    self.next(); // skip =
                                    body = self.parse_expr();
                                }

                                args.push(Arg {
                                    name,
                                    arg_type,
                                    body: Box::new(body),
                                });
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }

                self.next();

                let mut return_type = "void".to_string();

                // return type
                if Token::Special("->".to_string()) == self.peek() {
                    self.next();

                    if let Token::Identifier(ret_type) = self.peek() {
                        return_type = ret_type;
                    }
                    self.next();
                }

                return_node = AstNode::DefineFunction {
                    name,
                    return_type,
                    args,
                };
            }
        }

        return_node
    }

    fn parse_var(&mut self) -> AstNode {
        self.next();

        let name_token = self.peek();

        if let Token::Identifier(name) = name_token {
            self.next();

            if Token::Special(":".to_string()) == self.peek() {
                self.next();

                if let Token::Identifier(var_type) = self.peek() {
                    self.next();
                    if Token::Special("=".to_string()) == self.peek() {
                        self.next();

                        let expr = self.parse_expr();
                        return AstNode::DefVariable {
                            name,
                            var_type,
                            body: Box::new(expr),
                        };
                    } else {
                        return AstNode::DefVariable {
                            name,
                            var_type,
                            body: Box::new(AstNode::None),
                        };
                    }
                } else if Token::Special("=".to_string()) == self.peek() {
                    self.next();
                    let expr = self.parse_expr();

                    return AstNode::DefVariable {
                        name,
                        var_type: "unknown".to_string(),
                        body: Box::new(expr),
                    };
                }
            } else {
                panic!("dynamicly typed variables are not implemented")
            }
        }

        AstNode::None
    }

    fn parse_expr(&mut self) -> AstNode {
        self.parse_expr_bp(0)
    }

    // Prat parser
    fn parse_expr_bp(&mut self, min_bp: u8) -> AstNode {
        let mut lhs = self.parse_primary();

        loop {
            if self.tokens.len() <= self.pos {
                break;
            }

            let op = match self.peek() {
                Token::Special(op) => op,

                _ => break,
            };

            if let Some((l_bp, r_bp)) = self.infix_binding_power(&op) {
                if l_bp < min_bp {
                    break;
                }

                self.next();

                let rhs = self.parse_expr_bp(r_bp);

                lhs = AstNode::Binary {
                    op,
                    left: Box::new(lhs),
                    right: Box::new(rhs),
                };
                continue;
            }

            break;
        }

        lhs
    }

    fn prefix_binding_power(&self, op: &str) -> ((), u8) {
        match op {
            "+" | "-" | "!" => ((), 10),

            _ => panic!("bad op: {:?}", op),
        }
    }

    fn infix_binding_power(&self, op: &str) -> Option<(u8, u8)> {
        let res = match op {
            "&&" | "||" => (1, 2),

            ">" | "<" => (3, 4),
            "==" | "!=" => (3, 4),
            "<=" | ">=" => (3, 4),

            "^" => (5, 6),

            "+" | "-" => (5, 6),
            "*" | "/" => (7, 8),

            _ => return None,
        };

        Some(res)
    }

    fn parse_primary(&mut self) -> AstNode {
        let node = match self.peek() {
            Token::FloatingPointNumber(num) => AstNode::FloatingPointNumber(num),

            Token::Number(num) => AstNode::Number(num),

            Token::Identifier(identifier) => {
                let next = self.next();
                if Token::Special("(".to_string()) == next {
                    self.next();
                    if Token::Special(")".to_string()) == self.tokens[self.pos] {
                        self.next();
                        return AstNode::Call {
                            func_name: identifier,
                            args: vec![],
                        };
                    } else {
                        let mut args = Vec::<AstNode>::new();

                        loop {
                            let expr = self.parse_expr();
                            args.push(expr);

                            if Token::Special(",".to_string()) == self.peek() {
                                self.next();
                            } else {
                                break;
                            }
                        }
                        self.next();
                        //self.next();

                        return AstNode::Call {
                            func_name: identifier,
                            args,
                        };
                    }
                } else {
                    self.pos -= 1; //roll back next
                    AstNode::GetVariable { name: identifier }
                }
            }

            Token::Special(sp) => {
                if sp == "-" || sp == "+" {
                    let ((), r_bp) = self.prefix_binding_power(&sp);

                    self.next();

                    let rhs = self.parse_expr_bp(r_bp);
                    return AstNode::Binary {
                        op: "-".to_string(),
                        left: Box::new(AstNode::None),
                        right: Box::new(rhs),
                    };
                } else if sp == "(" {
                    self.next();
                    let lhs = self.parse_expr_bp(0);
                    self.next();
                    return lhs;
                } else {
                    AstNode::None
                }
            }

            _ => AstNode::None,
        };

        self.pos += 1;
        node
    }
}
