use std::char;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum RsvIdentifier {
    Func,
    Var,
    Return,
    Pass,

    If,
    Else,
    Elif,
    While,

    Break,
    Continue,

    For,
    In,

    Define,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    Reserved(RsvIdentifier),
    Identifier(String),
    String(String),
    Number(u64),
    FloatingPointNumber(f64),
    Special(String),
    //Comment(String),
    Ident,
    DeIdent,
    NewLine,
    EOF,

    Error,
    Skip,
}

pub struct Tokenizer {
    pos: usize,
    chars: Vec<char>,

    src: String,
}

impl Tokenizer {
    pub fn new(string: &String) -> Tokenizer {
        Tokenizer {
            pos: 0,
            chars: string.chars().collect(),
            src: string.to_string(),
        }
    }

    fn peek(&self) -> char {
        self.chars[self.pos]
    }

    fn next(&mut self) -> char {
        self.pos += 1;
        self.chars[self.pos]
    }

    fn is_at_the_end(&self) -> bool {
        self.chars.len() <= self.pos
    }

    pub fn parse(&mut self) -> Vec<Token> {
        let mut tokens = Vec::<Token>::new();

        let mut curent_indent = 0;
        loop {
            if self.is_at_the_end() {
                tokens.push(Token::EOF);
                break;
            }

            // Skip spaces
            loop {
                if self.is_at_the_end() {
                    tokens.push(Token::EOF);
                    return tokens;
                }

                if self.peek() != ' ' {
                    break;
                }

                self.pos += 1;
            }

            let start = self.pos;

            let token = match self.peek() {
                '#' => {
                    // Comment

                    let mut comment = String::new();

                    self.next();

                    loop {
                        if self.is_at_the_end() {
                            break;
                        }

                        let ch = self.peek();
                        self.next();

                        if ch == '\n' {
                            break;
                        }

                        comment.push(ch);
                    }
                    self.pos -= 1;

                    Token::Skip
                    //Token::Comment(comment)
                }

                '.' | '0'..='9' => {
                    let mut is_float = false;
                    let mut error = false;

                    loop {
                        if self.is_at_the_end() {
                            break;
                        }

                        let ch = self.peek();

                        if ch != '.' && !ch.is_numeric() {
                            break;
                        } else if ch == '.' {
                            if is_float {
                                // has 2 or more dots
                                error = true;
                                break;
                            }

                            is_float = true
                        }

                        self.next();
                    }

                    if !error {
                        if is_float {
                            Token::FloatingPointNumber(
                                self.src[start..self.pos].parse::<f64>().unwrap(),
                            )
                        } else {
                            Token::Number(self.src[start..self.pos].parse().unwrap())
                        }
                    } else {
                        Token::Error
                    }
                }

                'a'..='z' | 'A'..='Z' | '_' => {
                    // Parse identifier

                    let mut identifier = String::new();
                    //identifier.push(chars[pos]);

                    loop {
                        if self.is_at_the_end() {
                            break;
                        }

                        let ch = self.peek();

                        // A word-like identifier only contains underscores and alphanumeric characters.
                        if ch == '_' || ch.is_alphanumeric() {
                            identifier.push(ch);
                            self.pos += 1;
                        } else {
                            break;
                        }
                    }

                    match identifier.as_str() {
                        "func" => Token::Reserved(RsvIdentifier::Func),
                        "var" => Token::Reserved(RsvIdentifier::Var),
                        "return" => Token::Reserved(RsvIdentifier::Return),
                        "pass" => Token::Reserved(RsvIdentifier::Pass),
                        "if" => Token::Reserved(RsvIdentifier::If),
                        "else" => Token::Reserved(RsvIdentifier::Else),
                        "elif" => Token::Reserved(RsvIdentifier::Elif),
                        "while" => Token::Reserved(RsvIdentifier::While),
                        "break" => Token::Reserved(RsvIdentifier::Break),
                        "continue" => Token::Reserved(RsvIdentifier::Continue),
                        "in" => Token::Reserved(RsvIdentifier::In),
                        "for" => Token::Reserved(RsvIdentifier::For),
                        "define" => Token::Reserved(RsvIdentifier::Define),

                        "or" => Token::Special("||".to_string()),
                        "and" => Token::Special("&&".to_string()),

                        _ => Token::Identifier(identifier),
                    }
                }

                '\n' => {
                    let mut new_indent: f32 = 0.0;

                    if self.chars.len() - 1 > self.pos {
                        self.next();
                    }

                    loop {
                        if self.is_at_the_end() {
                            break;
                        }

                        if self.peek() == ' ' || self.peek() == '\t' {
                            new_indent += 0.25; // 1 space is 1/4 of the tab
                        } else if self.peek() == '\n' {
                            new_indent = 0.0;
                        } else if self.peek() == '#' {
                            loop {
                                self.next();
                                if self.peek() == '\n' {
                                    new_indent = 0.0;
                                    break;
                                }
                            }
                        } else {
                            break;
                        }

                        if self.chars.len() - 1 <= self.pos {
                            break;
                        }
                        self.next();
                    }

                    let new_indent = new_indent.ceil() as i64;

                    if new_indent > curent_indent {
                        tokens.push(Token::NewLine);

                        for _i in curent_indent..new_indent {
                            tokens.push(Token::Ident);
                        }
                        curent_indent = new_indent;

                        Token::Skip
                    } else if new_indent < curent_indent {
                        tokens.push(Token::NewLine);

                        for _i in new_indent..curent_indent {
                            tokens.push(Token::DeIdent);
                        }
                        curent_indent = new_indent;

                        Token::Skip
                    } else {
                        Token::NewLine
                    }
                }

                ';' => Token::NewLine,

                _ => {
                    let posiable_specials = [
                        "(", ")", ":", ",", ".", "=", "->", "+", "-", "*", "/", "<", ">", "!", "&",
                        "|", "~", "%", "^=", "<<", ">>", "&&", "||", "+=", "-=", "*=", "/=", "<=",
                        ">=", "!=", "%=", "&=", "|=", "==", "<<=", ">>=",
                    ];

                    let mut str = String::new();
                    str.push(self.chars[self.pos]);

                    if self.chars.len() != self.pos + 1 {
                        str.push(self.chars[self.pos + 1]);
                    }

                    if self.chars.len() != self.pos + 2 {
                        str.push(self.chars[self.pos + 2]);
                    }

                    let mut ret_tok = Token::Error;

                    if posiable_specials.contains(&str.as_str()) {
                        ret_tok = Token::Special(str.clone());
                        self.pos += 3;
                    } else {
                        str.remove(str.len() - 1);

                        if posiable_specials.contains(&str.as_str()) {
                            ret_tok = Token::Special(str.clone());
                            self.pos += 2;
                        } else {
                            str.remove(str.len() - 1);

                            if posiable_specials.contains(&str.as_str()) {
                                ret_tok = Token::Special(str.clone());
                                self.pos += 1;
                            }
                        }
                    }

                    ret_tok
                }
            };

            if start == self.pos {
                self.pos += 1;
            }

            if token == Token::EOF {
                break;
            }

            if token != Token::Skip {
                tokens.push(token);
            }
        }

        tokens
    }
}
