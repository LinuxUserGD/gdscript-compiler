use crate::{compiler::Compiler, parser::Parser};
use inkwell::{context::Context, passes::PassManager, OptimizationLevel};

pub mod compiler;
pub mod parser;
pub mod tokenizer;

use tokenizer::Tokenizer;

fn main() {
    std::env::set_var("RUST_BACKTRACE", "1");

    let code = std::fs::read_to_string("./src/main.gd").unwrap();

    let tokens = Tokenizer::new(&code).parse();

    let nodes = Parser::new(&tokens).parse();

    let context = Context::create();
    let builder = context.create_builder();
    let module = context.create_module("my_module");

    let fpm = PassManager::create(&module);

    let mut cmp = Compiler::new(false, &context, &builder, &module, &fpm);

    cmp.compile_all(&nodes);

    cmp.add_gdextension();

    cmp.write_to_file(OptimizationLevel::None, "./output/gdscript.o");

    let mut use_lib = false;
    let mut run = false;
    let mut use_jit = false;

    for arg in std::env::args() {
        match arg.as_str() {
            "-h" => println!(
                "you can add any of these \n --print-intput \n --print-tokens \n --print-ast"
            ),
            "--print-intput" => println!("compiling code:\n \n{}\n", code),
            "--print-tokens" => println!("Tokens: \n{:?} \n", tokens),
            "--print-ast" => println!("Parse tree: \n{:#?}", nodes),
            "--print-ir" => cmp.print_ir(),
            "--use-lib" => use_lib = true,
            "--run" => run = true,
            "--jit" => use_jit = true,
            _ => {}
        }
    }

    let mut gcc_cmd = "gcc -o ./output/output ./output/gdscript.o".to_string();
    if use_lib {
        gcc_cmd += " ./output/lib.c";
    }

    let status = std::process::Command::new("sh")
        .args(["-c", &gcc_cmd])
        .status();

    if status.is_ok() {
        println!();

        if run {
            let exit_code;

            if use_jit {
                cmp.run_with_jit();
                return;
            } else if cfg!(target_os = "windows") {
                println!("Windows is not supported to compiling to executable running jit");
                cmp.run_with_jit();
                return;
            } else {
                exit_code = std::process::Command::new("./output/output")
                    .status()
                    .expect("faild compiling to executable")
            }

            println!("{}", exit_code);
        }
    } else {
        println!("gcc faild compiling to executable");

        if run {
            cmp.run_with_jit();
        }
    }
}

#[cfg(test)]
mod unit_tests;
