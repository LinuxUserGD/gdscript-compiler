func main() -> int:
    var result := 0

    for i in range(1):
        if true:
            break
        result += 1
    
    for i in range(0, 1):
        result += 1
    
    for i in range(0, 1, 1):
        result += 1
    
    for i in 1:
        result += 1
    
    return result