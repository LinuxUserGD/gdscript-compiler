func func1() -> int:
    return 1 + func_with_arg1(1)


func func_with_arg1(arg: int) -> int:
    func_with_arg2(arg)
    return arg

func func_with_arg2(arg: int):
    pass


func func_with_args1(arg1: int, arg2: float) -> int:
    func_with_args2(arg1, arg2)
    return arg1

func func_with_args2(arg1: int, arg2: float):
    pass

func function_with_defualt_values(arg : int = 1) -> int:
    return arg

func main() -> int:
    var i := 0
    i += func1()
    i += func_with_args1(1, 0.0)
    i += function_with_defualt_values()
    i += function_with_defualt_values(2)

    return i
