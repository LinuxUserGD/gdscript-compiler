func main() -> int:
    var i := 0
    
    if false:
        i -= 100
    elif true:
        i += 1
    elif true:
        i -= 100
    else:
        i -= 100
    
    if false:
        i -= 100
    else:
        i += 1
    
    if false:
        i -= 100
    
    return i