use inkwell::context::Context;
use inkwell::passes::PassManager;

use crate::parser::AstNode;
use crate::Compiler;
use crate::{parser::Parser, tokenizer::Tokenizer};
//use crate::unit_tests::AstNode::*;
//use std::vec;

fn compile(nodes: Vec<AstNode>) -> i64 {
    let context = Context::create();
    let builder = context.create_builder();
    let module = context.create_module("my_module");

    let fpm = PassManager::create(&module);

    fpm.initialize();

    let mut cmp = Compiler::new(false, &context, &builder, &module, &fpm);

    cmp.compile_all(&nodes);
    cmp.run_with_jit()
}

#[test]
fn functions() {
    //std::env::set_var("RUST_BACKTRACE", "1");

    let code = std::fs::read_to_string("./src/unit_tests/gdscript_code/functions.gd").unwrap();
    let tokens = Tokenizer::new(&code).parse();
    let nodes = Parser::new(&tokens).parse();

    let output = compile(nodes);

    assert_eq!(output, 6);
}

#[test]
fn if_test() {
    //std::env::set_var("RUST_BACKTRACE", "1");

    let code = std::fs::read_to_string("./src/unit_tests/gdscript_code/if.gd").unwrap();
    let tokens = Tokenizer::new(&code).parse();
    let nodes = Parser::new(&tokens).parse();

    let output = compile(nodes);

    assert_eq!(output, 2);
}

#[test]
fn for_test() {
    //std::env::set_var("RUST_BACKTRACE", "1");

    let code = std::fs::read_to_string("./src/unit_tests/gdscript_code/for loops.gd").unwrap();
    let tokens = Tokenizer::new(&code).parse();
    let nodes = Parser::new(&tokens).parse();

    let output = compile(nodes);

    assert_eq!(output, 3);
}

#[test]
fn math_test() {
    //std::env::set_var("RUST_BACKTRACE", "1");

    let code = std::fs::read_to_string("./src/unit_tests/gdscript_code/math.gd").unwrap();
    let tokens = Tokenizer::new(&code).parse();
    let nodes = Parser::new(&tokens).parse();

    let output = compile(nodes);

    assert_eq!(output, 25);
}

#[test]
fn while_test() {
    //std::env::set_var("RUST_BACKTRACE", "1");

    let code = std::fs::read_to_string("./src/unit_tests/gdscript_code/while loops.gd").unwrap();
    let tokens = Tokenizer::new(&code).parse();
    let nodes = Parser::new(&tokens).parse();

    let output = compile(nodes);

    assert_eq!(output, 5);
}
