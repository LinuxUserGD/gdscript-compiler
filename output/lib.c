#include <stdio.h>

#include <stdint.h>
#include <inttypes.h>

void print(int64_t i){
    printf("%" PRId64 "\n", i);
}
